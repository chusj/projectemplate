第1，在根目录下新建 content文件夹
第2，将整体代码copy到此文件夹下，注意删除obj、bin文件夹
第3，.gitignore文件默认排除了content文件夹



查看模板：
dotnet new -l
dotnet new list

可查看本地器已安装的模板
dotnet new install 

--安装
dotnet new -i BCVP.Net8.Template

--卸载
dotnet new -u BCVP.Net8.Template



--安装本地文件
dotnet new install 本地磁盘路径\xxx.nupkg

--举例：
dotnet new -i D:\Users\chusj-gitee\projectemplate\Seehealth.Webapi.Template.24.1.1.nupkg
